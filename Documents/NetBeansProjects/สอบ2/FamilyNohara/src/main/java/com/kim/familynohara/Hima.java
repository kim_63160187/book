/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.familynohara;

/**
 *
 * @author ASUS
 */
//กำหนดว่า class Huma เป็นคลาสลูกของ FamilyNoha โดยใช้ extends
public class Hima extends FamilyNoha{
    
    public Hima(String name,int age,String sex, String job){
        // super ใน class Hima มี name,age,sex,job พวกนี้เป็นของ FamilyNoha
        super(name,age,sex,job);
           System.out.println("<Hima Create>"); 
    }
    
    //การOverride Method ข้อมูลฮิมา
    @Override
    public void getDetails() {
        System.out.println("Name :" + " "+ this.name + " "+ ",Age :" + " "+this.age + " "+ "years"  + " " 
        +",Sex :" +" "+ sex  + " " + ",Job :" + " "+ job);
     }
}
