/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.familynohara;

/**
 *
 * @author ASUS
 */

//กำหนดว่า class Mom เป็นคลาสลูกของ FamilyNoha โดยใช้ extends
public class Mom extends  FamilyNoha{
    
    public Mom(String name,int age,String sex, String job){
        // super ใน class Mom มี name,age,sex,job พวกนี้เป็นของ FamilyNoha
        super(name,age,sex,job);
           System.out.println("<Mom Create>"); 
    }
    
    //การOverride Method ข้อมูลแม่
    @Override
    public void getDetails() {
        System.out.println("Name :" + " "+ this.name + " "+ ",Age :" + " "+this.age + " "+ "years"  + " " 
        +",Sex :" +" "+ sex  + " " + ",Job :" + " "+ job);
     }
}
